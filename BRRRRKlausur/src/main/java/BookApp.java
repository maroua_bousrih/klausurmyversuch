import at.campus02.ase.books.Bibliothek;
import at.campus02.ase.books.Book;

public class BookApp
{

	public static void main(String[] args)
	{
		
		
		Bibliothek bibi = new Bibliothek();
		
		
		Book javabook= new Book("maroua","javaBases",680);
		Book cbook  = new Book("bassem","Cforeverxday",520);
		Book web = new Book("maroua","web design", 362);
		
		bibi.addBook(javabook);
		bibi.addBook(cbook);
		bibi.addBook(web);
		
		System.out.println(bibi.countPages());
		
		
		
	}

}
