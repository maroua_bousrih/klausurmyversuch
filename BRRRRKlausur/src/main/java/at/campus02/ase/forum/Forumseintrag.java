package at.campus02.ase.forum;

import java.util.ArrayList;

public class Forumseintrag {

	private String titel;
	private String text;
	ArrayList<Forumseintrag> listantworten=new ArrayList<Forumseintrag>();

	public Forumseintrag(String titel, String text) {
		this.titel=titel;
		this.text=text;
	}

	public Forumseintrag antworten(String titel, String text) {
		
		Forumseintrag forumseintrag = new Forumseintrag(titel, text);
		
		listantworten.add(forumseintrag);
		
		return forumseintrag;
		
			
	}

	public ArrayList<Forumseintrag> getAntworten() {
		return listantworten;
	
//		ArrayList<Forumseintrag> display = new ArrayList<Forumseintrag>();
//				
//		for (Forumseintrag f: listantworten)
//		{
//		  display.add(f);
//			
//		}
//		return display;
	}

	public int anzahlDerEintraege()
	{
		int result=0;
		for(Forumseintrag f: listantworten)
		{  
		     result = result+ 1 + f.anzahlDerEintraege();   
		}
		
		return result;
	
		
	}
	
	public String toString()
	{
		String antworten = "";
		for (Forumseintrag forumseintrag : listantworten) {
			antworten = antworten + forumseintrag.toString();
		}
		if (!antworten.isEmpty())
			antworten = "[" + antworten + "]";
		
		return String.format("(%s,%s)%s", titel, text, antworten);
		
	}
}
