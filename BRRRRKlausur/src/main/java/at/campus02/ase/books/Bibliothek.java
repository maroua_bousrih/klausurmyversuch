package at.campus02.ase.books;

import java.util.ArrayList;

public class Bibliothek {
	ArrayList<Book> biblio = new ArrayList<Book>();

	public void addBook(Book b) 
	{
	biblio.add(b);	
		
	}

	public int countPages() {
		int sum=0;
		for(Book b:biblio)
		{
			sum =sum + b.getSeiten();
		}
		return sum;
	}

	public double avaragePages() {
		double average=0;
		double page=countPages();
		average= page/biblio.size();
		return average;
	}

	public ArrayList<Book> booksByAuthor(String autor) {
		
		ArrayList<Book> books =new ArrayList<Book>();
		for (Book b:biblio)
		{
			String x= b.getAutor();
			if(x.equalsIgnoreCase(autor))
			{
				books.add(b);
				
			}
		
						
		}
		return books;
		
	}

	public ArrayList<Book> findBook(String search)
	{
		ArrayList<Book> find= new ArrayList<Book>();
		
		if(search == null)
		{
			return find;
		}
		else
		{
		for(Book b:biblio)
		{
			if(b.match(search)==true)
			{
			find.add(b);
			}
			
		}
		return find;
		}
	}

}
